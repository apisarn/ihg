package com.daimajia.slider.demo;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.media.AudioManager;
import android.media.RemoteControlClient;
import android.media.RemoteController;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.daimajia.slider.demo.util.DFragment;
import com.hkm.slider.SliderTypes.BaseSliderView;
import com.hkm.slider.Tricks.ViewPagerEx;

public class MainActivity extends ActionBarActivity implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener, View.OnClickListener {

    private static final java.lang.String URL_HOME_PAGE = "http://113.32.157.114:51102/hotel/ss/index.php?id=2";
    private static final java.lang.String URL_GUEST_SERVICES_PAGE = "http://113.32.157.114:51102/hotel/gs/index.php?id=2";
    private static final java.lang.String URL_WEATHER_PAGE = "https://weather.yahoo.com/";

    private FragmentManager fm = getSupportFragmentManager();
    private Button aBtnHome;
    private Button aBtnGuestServices;
    private Button aBtnWeather;
    private Button aBtnChromeBrowser;
    private Button aBtnGooglePlay;
    private ImageView aIvExit;
    private WebView aWvMain;



    @Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(this, slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }


    public void onBackPressed() {
        ;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        aWvMain = (WebView) findViewById(R.id.wvMain);
        aIvExit = (ImageView) findViewById(R.id.iv_exit);
        aBtnHome = (Button) findViewById(R.id.btn_home);
        aBtnGuestServices = (Button) findViewById(R.id.btn_guest_services);
        aBtnWeather = (Button) findViewById(R.id.btn_weather);
        aBtnChromeBrowser = (Button) findViewById(R.id.btn_chrome_browser);
        aBtnGooglePlay = (Button) findViewById(R.id.btn_google_play);

        aIvExit.setOnClickListener(this);
        aBtnHome.setOnClickListener(this);
        aBtnGuestServices.setOnClickListener(this);
        aBtnWeather.setOnClickListener(this);
        aBtnChromeBrowser.setOnClickListener(this);
        aBtnGooglePlay.setOnClickListener(this);


        int[][] states = new int[][] {
           new int[] {android.R.attr.state_focused},
           new int[] {android.R.attr.state_pressed},
           new int[] {android.R.attr.state_selected},
           new int[] {android.R.attr.state_hovered},
                new int[] {}
        };

        int[] colors = new int[] {
                getResources().getColor(R.color.black),
                getResources().getColor(R.color.black),
                getResources().getColor(R.color.black),
                getResources().getColor(R.color.black),
                getResources().getColor(R.color.white)
        };

        ColorStateList colorStateList = new ColorStateList(states, colors);


        aBtnHome.setTextColor(colorStateList);
        aBtnGuestServices.setTextColor(colorStateList);
        aBtnWeather.setTextColor(colorStateList);
        aBtnChromeBrowser.setTextColor(colorStateList);
        aBtnGooglePlay.setTextColor(colorStateList);

        aWvMain.getSettings().setJavaScriptEnabled(true);
        aWvMain.setWebChromeClient(new WebChromeClient());
        aWvMain.setWebViewClient(new DotComWebView());

        aWvMain.loadUrl(URL_HOME_PAGE);

        // make home button selected
        setButtonAllUnselect();
        aBtnHome.setSelected(true);
        aBtnHome.requestFocus();
        aBtnHome.setTextColor(getResources().getColor(R.color.black));

    }

    class MyEventControllerListener extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {


            Toast.makeText(getApplicationContext(), intent.getAction(), Toast.LENGTH_SHORT).show();
        }
    }


    private class DotComWebView extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        //        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private void setButtonAllUnselect() {
        aBtnHome.setSelected(false);
        aBtnGuestServices.setSelected(false);
        aBtnWeather.setSelected(false);
        aBtnChromeBrowser.setSelected(false);
        aBtnGooglePlay.setSelected(false);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_exit: {
                DFragment dFragment = new DFragment();
                dFragment.show(fm, "Dialog Fragment");
                break;
            }

            case R.id.btn_home:{
                aWvMain.loadUrl(URL_HOME_PAGE);
                setButtonAllUnselect();
                aBtnHome.setSelected(true);
                aBtnHome.setTextColor(getResources().getColor(R.color.black));
                break;
            }
            case R.id.btn_guest_services:{
                aWvMain.loadUrl(URL_GUEST_SERVICES_PAGE);
                setButtonAllUnselect();
                aBtnGuestServices.setSelected(true);
                aBtnGuestServices.setTextColor(getResources().getColor(R.color.black));
                break;
            }

            case R.id.btn_weather:{
                aWvMain.loadUrl(URL_WEATHER_PAGE);
                setButtonAllUnselect();
                aBtnWeather.setSelected(true);
                aBtnWeather.setTextColor(getResources().getColor(R.color.black));
                break;
            }
            case R.id.btn_chrome_browser:{


                String url = "http://www.google.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

                break;
            }

            case R.id.btn_google_play: {

                String url = "https://play.google.com/store?hl=en";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

                break;
            }



            default:
                break;
        }
    }
}
